import turtle
import math
import random

s = turtle.getscreen()

t = turtle.turtles()[0]

def polarToCartesian(radius, degrees):
    rads = math.radians(degrees)
    return [radius * math.cos(rads), radius * math.sin(rads)]

def p2c(radius, degrees):
  return polarToCartesian(radius, degrees)

def draw_square(size, x=0, y=0):
  t.up()
  t.goto(x + -size/2,y + -size/2)
  t.down()

  for i in range(4):
    t.forward(size)
    t.left(90)

def draw_shape(sides):
  for i in range(sides):
    t.forward(50)
    t.left(360/sides)

def draw_polar_shape(sides, radius, x=0, y=0):
  angle = int(360/sides)
  t.up()

  for i in range(0, 360 + angle, angle):
    coords = polarToCartesian(radius, i)
    t.goto(coords[0] + x, coords[1] + y)
    t.down()

# for i in range(0, 360*4, 10):
#   coords = polarToCartesian(i/12, i)
#   t.fillcolor((0, 1.0 - i/(360*4),i/(360*4)))
#   t.begin_fill()
#   draw_polar_shape(8, i/45, coords[0], coords[1])
#   t.end_fill()

for i in range (0, 360*4, 10):
  coords = polarToCartesian(i/12,i)
  t.fillcolor((0  , 1.0- i/(360*4),   i/(360*4)  ))
  t.begin_fill()
  draw_polar_shape(4,i/45, coords[0], coords[1])
  t.end_fill()

