# Import turtle graphics library
import turtle

# Creat and hold screen
s = turtle.getscreen()

# Create and hold the turtle drawing object
t = turtle.Turtle()

# Basic Movement
# t.forward(100)  # move forward 100 pixels
# t.left(90)      # turn 90 degrees left
# t.right(90)     # turn 90 degrees right
# t.up()          # stop drawing
# t.down()        # resume drawing
# t.goto(50,20)   # move directly to xy coords (no turning)
# t.circle(40)    # draws a circle on the current point of 40 radius
# t.dot(5)        # draws a dot with diameter of 5
# t.pensize(5)    # sets the thickness of the drawn line

def draw_square():
    


# Instruct the window to stay open until we click on it to close
s.exitonclick()