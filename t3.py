import turtle
from math import pi, sin, cos, radians
import random


s = turtle.getscreen()

t = turtle.turtles()[0]
t.hideturtle()

def polarToCartesian(radius, degrees):
    rads = radians(degrees)
    return (radius * cos(rads), radius * sin(rads))

def draw_archimedes_spiral():
    t.speed(0)
    t.up()
    for i in range(1000):
        z = i / 33 * pi
        dx = (1 + 5 * z) * cos(z)
        dy = (1 + 5 * z) * sin(z)

        t.goto(dx, dy)
        t.dot(2)

def draw_pentagram():
    t.up()
    for i in range(0, 12, 2):
        t.goto(polarToCartesian(100, (360/5)*i))
        t.down()

def draw_wobbly_circle(radius, roughness, resolution):
    t.fillcolor("grey")
    t.up()
    first = None
    t.begin_fill()
    for i in range(0, 360, resolution):
        t.goto(polarToCartesian(random.randrange(radius - roughness, radius + roughness), i))
        if (i == 0):
            first = t.pos()
        t.down()
    t.goto(first)
    t.end_fill()
    

#draw_archimedes_spiral()
draw_wobbly_circle(100, 5, 7)

s.exitonclick()



# Want to make points equidistant? Refer to 
# https://stackoverflow.com/questions/13894715/draw-equidistant-points-on-a-spiral