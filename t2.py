# Import turtle graphics library
import turtle

# Import random library for draw_random function
import random

# Create and hold screen
s = turtle.getscreen()

# Grab the first, initial turtle
t = turtle.turtles()[0]


def draw_square(size):
    for i in range(0, 4):
        t.forward(size)
        t.left(90)


def draw_centered_square(size):
    t.up()
    t.goto(-size/2, -size/2)
    t.down()
    draw_square(size)


def draw_shape(sides, length=30):
    for i in range(0, sides):
        t.forward(length)
        t.left(360/sides)


def draw_ouroboros():
    for i in range(0, 40):
        t.forward(20)
        t.left(360/40)
        t.pensize(i)


def draw_concentric_circles(count):
    for i in range(0, count):
        # Move to start position (0, -radius)
        t.speed(50)
        t.up()
        t.goto(0, -(10*(i+1)))
        t.down()

        # draw circle
        t.speed(10)
        t.circle(10*(i+1))


def draw_random():
    for i in range(100):
        t.forward(random.randint(2, 30))
        t.left(random.randint(0, 360))


def draw_random2():
    t.speed(10)
    for i in range(0, 10000):
        t.goto(random.randint(-200, 200), random.randint(-200, 200))

def draw_pentagram():
    for i in range(0, 5):
        t.forward(100)
        t.left(144)



draw_pentagram()

# Instruct the window to stay open until we click on it to close
s.exitonclick()
