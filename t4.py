import turtle
from math import pi, sin, cos, radians, floor
import random
from PIL import Image

im = Image.open("audrey400.png")
pix = im.load()
#print(im.size)

s = turtle.getscreen()

t = turtle.turtles()[0]
t.hideturtle()
t.speed(0)

# Convert Polar Coordinates To Cartesian
def polarToCartesian(radius, degrees):
    rads = radians(degrees)
    return (radius * cos(rads), radius * sin(rads))

# Turtle coords are -400 - 400 on both axis, image coords are 0 - 400 and y starts from the top 
def turtleToImageCoords(tCoords):
    return (tCoords[0] + 400)/2, 399 - (tCoords[1] + 400)/2


def draw_pic_with_dots():
    t.up()
    for y in range(-400, 400, 20):
        for x in range(-400, 400, 20):
            print(str(x) + ", " + str(y))
            t.goto(x, y)
            _coords = turtleToImageCoords([x,y])
            print("_ " + str(_x) + ", " + str(_y))
            brightness = 8-(pix[_coords][0]/32)
            # #print(str(x) + ", " + str(y))
            t.dot(brightness)


# Draws an archimedes spiral from equidistant points, alternating between moving slightly in
# or out based on the darkness of the underlying pixels
def draw_image_on_archimedes_spiral(spiralDensity = 0.2, pointDensity = 110):
    r = 5 # start radius
    b = spiralDensity / (2 * pi)
    # find the first phi to satisfy distance of `arc` to the second point
    phi = float(r) / b

    # alternates -1 to 1 do indicate the current direction of the deviation
    isOdd = 1

    while True:
        if r > 380:
            print("hit max radius")
            break
        # Get the cartesian coordiates of the next point
        cart = polarToCartesian(r, phi)        

        # map the turtle coordinates to the 400x400px image coordiates
        imageCoords = turtleToImageCoords(cart)    

        deviation = (pix[imageCoords][0] +  pix[imageCoords][1] + pix[imageCoords][2]) / 3 # get the 0-255 brightness of the underlying pixel
        deviation = 4 - (deviation / 64.0) # map the 0-255 number to 0-8
        deviation = floor(deviation) # OPTIONAL: round the 0-8 value down so low numbers drop to zero for smooth lines in quite white areas
        
        cartWithDeviation = polarToCartesian(r + (deviation * isOdd), phi)
        isOdd = -isOdd
        
        t.goto(cartWithDeviation)
        #t.dot(2)

        # advance the variables
        # calculate phi that will give desired arc length at current radius
        # (approximating with circle)
        phi += float(pointDensity) / r
        r = b * phi

draw_image_on_archimedes_spiral()

s.exitonclick()